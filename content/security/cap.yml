---
  title: 'GitLab Customer Assurance Package'
  og_itle: 'GitLab Customer Assurance Package'
  description: At GitLab, we're committed to Information Security. It is our mission to be the most transparent Security orgnization in the world.
  twitter_description: At GitLab, we're committed to Information Security. It is our mission to be the most transparent Security orgnization in the world.
  og_description: At GitLab, we're committed to Information Security. It is our mission to be the most transparent Security orgnization in the world.
  components:
    - name: 'security-content-block'
      data:
        padded: true
        note:
          text: Customer Assurance Package
        header:
          text: At GitLab, your data is protected
          typography: heading1-bold
        column_size: 10
        description:
          text: |
                We believe that transparency is critical to our success. We want all GitLab customers to be empowered with confidence and trust that their data is protected.
        divider: true
    - name: 'security-content-block'
      data:
        header:
          text: Stay current on security best practices
          typography: heading2-bold
        column_size: 7
        description:
          text: |
                Our Customer Assurance Packages (CAPs) are designed to provide GitLab customers and community members with self-serve access to the most current information about our Security and Compliance posture. Whether completing a GitLab.com security assessment or just wanting to learn more about GitLab security practices, this is your one stop shop.<br>
                For more details about GitLab's trust practices, visit the [GitLab Trust Center](/security/).
          typography: body1
        image:
          image_url: /nuxt-images/security/cap/cap-hero.jpeg
          alt: "GitLab team members"
          bordered: true
    - name: 'security-package-cards'
      data:
        column_size: 6
        cards:
          - title: Community Package
            description: The first step on the trust journey, this package is a compilation of publicly available documentation designed to introduce GitLab’s approach to security.
            button:
              url: https://about.gitlab.com/resources/customer-assurance-package/gitlab-cap-current.zip
              text: Download now
              data_ga_name: download now
              data_ga_location: body
              icon:
                name: arrow-down
                alt: Arrow Down Icon
                variant: product
            note: Continuously updated, check back often
            list:
              - text: December 2020 GitLab.com SOC 3 Report
              - text: October 2021 GitLab.com SOC 3 Report
              - text: ISO/IEC 27001:2013 Certificate
                url: https://www.schellman.com/certificate-directory?certificateNumber=1652216-2
                data_ga_name: iso/iec 27001:2013 certificate
                data_ga_location: body
              - text: CSA CAIQ Level 1 GitLab.com Questionnaire
                url: https://cloudsecurityalliance.org/star/registry/services/gitlab
                data_ga_name: csa caiq level 1 questionnaire
                data_ga_location: body
              - text: Standard Information Gathering (SIG) Core GitLab.com Questionnaire
              - text: BitSight Security Report for GitLab.com
              - text: 'GitLab Technical Report: Securing Customer Data'
              - text: "GitLab Technical Report: Securing GitLab's Supply Chain"
              - text: ISO/IEC 20243-1:2018 Self Assessment
                url: https://certification.opengroup.org/register/ottps-certification
                data_ga_name: iso/iec 20243-1:2018 self assessment
                data_ga_location: body
              - text: FIPS 140-2 Attestation
          - title: GitLab.com Package
            description: This package provides detailed security information to prospective and existing GitLab.com SaaS and self-managed customers for completing vendor security assessments. Due to the sensitive nature of the documentation, an NDA is required to be in place prior to sharing.
            button:
              url: 'mailto:customer-assurance@gitlab.com'
              text: Request by Email
              data_ga_name: request by email
              data_ga_location: body
              icon:
                name: mail
                alt: mail Icon
                variant: product
            note: Or send a request directly to your account manager
            list:
              - text: December 2020 SOC 2 Type 2 Report
              - text: October 2021 SOC 2 Type 2 Report and Bridge Letter
              - text: Annual GCP SOC 3 Report (GitLab.com Hosting Provider)
              - text: Annual PCI DSS SAQ-A Self-Assessment
              - text: Annual GitLab Business Continuity Test Executive Summary
              - text: Annual Third Party Penetration Test Executive Summary
              - text: ISO/IEC 27001:2013 Customer Summary Letter
              - text: Transfer Impact Assessment Guide for Customers
              - text: TISAX Self-Attestation
          - title: GitLab Dedicated Package
            description: This package provides detailed security information to prospective and existing GitLab Dedicated customers for completing vendor security assessments. Due to the sensitive nature of the documentation, an NDA is required to be in place prior to sharing.
            button:
              url: 'mailto:customer-assurance@gitlab.com'
              text: Request by Email
              data_ga_name: request by email
              data_ga_location: body
              icon:
                name: mail
                alt: mail Icon
                variant: product
            note: Or send a request directly to your account manager
            info_tag: Coming soon!
          - title: U.S. Public Sector Package
            description: This package provides detailed security information to prospective and existing U.S. Public Sector customers for completing vendor security assessments. Due to the sensitive nature of the documentation, an NDA is required to be in place prior to sharing.
            button:
              url: 'mailto:customer-assurance@gitlab.com'
              text: Request by Email
              data_ga_name: request by email
              data_ga_location: body
              icon:
                name: mail
                alt: mail Icon
                variant: product
            note: Or send a request directly to your account manager
            info_tag: Coming soon!
    - name: 'security-resources-links'
      data:
        title: GitLab's Key Policies and Procedures
        mobile_title : Resources
        group_items: true
        divider: true
        items:
          - header: GitLab's Key Policies and Procedures
            items:
              - items:
                - text: 'Access Management Policy'
                  link: https://about.gitlab.com/handbook/security/access-management-policy.html
                  data_ga_name: access management policy
                  data_ga_location: body
                - text: 'Application Architecture'
                  link: https://docs.gitlab.com/ee/development/architecture/
                  data_ga_name: application architecture
                  data_ga_location: body
                - text: 'Audit Logging Policy'
                  link: https://about.gitlab.com/handbook/security/audit-logging-policy.html
                  data_ga_name: audit logging policy
                  data_ga_location: body
                - text: 'Business Continuity Plan'
                  link: https://about.gitlab.com/handbook/business-ops/gitlab-business-continuity-plan/
                  data_ga_name: business continuity plan
                  data_ga_location: body
                - text: 'Data Classification Standard'
                  link: https://about.gitlab.com/handbook/security/data-classification-standard.html
                  data_ga_name: data classification standard
                  data_ga_location: body
              - items:
                  - text: 'Data Protection Impact Assessment Policy'
                    link: https://about.gitlab.com/handbook/legal/privacy/dpia-policy/
                    data_ga_name: data protection impact assessment policy
                    data_ga_location: body
                  - text: 'Information Security Management System'
                    link: https://about.gitlab.com/handbook/security/ISMS.html
                    data_ga_name: information security management system
                    data_ga_location: body
                  - text: 'Internal Acceptable Use Policy'
                    link: https://about.gitlab.com/handbook/people-group/acceptable-use-policy/
                    data_ga_name: internal acceptable use policy
                    data_ga_location: body
                  - text: 'Network Architecture'
                    link: https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/#network-architecture/
                    data_ga_name: network architecture
                    data_ga_location: body
                  - text: 'Password Policy'
                    link: https://about.gitlab.com/handbook/security/#gitlab-password-policy-guidelines
                    data_ga_name: password policy
                    data_ga_location: body
              - items:
                  - text: 'Penetration Testing Policy'
                    link: https://about.gitlab.com/handbook/security/penetration-testing-policy.html
                    data_ga_name: penetration testing policy
                    data_ga_location: body
                  - text: 'Production Architecture'
                    link: https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/
                    data_ga_name: production architecture
                    data_ga_location: body
                  - text: 'Security Control Framework'
                    link: https://about.gitlab.com/handbook/security/security-assurance/security-compliance/sec-controls.html
                    data_ga_name: security control framework
                    data_ga_location: body
                  - text: 'Security Incident Response Guide'
                    link: https://about.gitlab.com/handbook/security/security-operations/sirt/sec-incident-response.html
                    data_ga_name: security incident response guide
                    data_ga_location: body
                  - text: 'Vulnerability Management Policy'
                    link: https://about.gitlab.com/handbook/security/threat-management/vulnerability-management/
                    data_ga_name: vulnerability management policy
                    data_ga_location: body

    - name: 'security-cta-section'
      data:
        cards:
          - title: Reach out to our Security Team if you have questions or concerns
            icon:
              name: mail
              slp_color: surface-700
            link:
              text: Contact our Security Team
              url: mailto:customer-assurance@gitlab.com
              data_ga_name: contact our security team
              data_ga_location: body
          - title: Get security release notifications and alerts delivered to your inbox
            icon:
              name: paper-ariplane
              slp_color: surface-700
            link:
              text: Sign up for security notices
              url: https://about.gitlab.com/company/contact/
              data_ga_name: sign up for security notices
              data_ga_location: body
    - name: 'solutions-cards'
      data:
        title: Security solutions with GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Explore more Solutions
          data_ga_name: soluions explore more
          data_ga_location: body
        cards:
          - title: DevSecOps
            description: GitLab empowers your teams to balance speed and security by automating software delivery and securing your end-to-end software supply chain.
            icon:
              name: devsecops
              alt: Devsecops Icon
              variant: marketing
            href: /solutions/dev-sec-ops/
            data_ga_name: devsecpps learn more
            data_ga_location: body
          - title: Continuous Software Compliance
            description: Integrating security into your DevOps lifecycle is easy with GitLab.
            icon:
              name: build
              alt: build Icon
              variant: marketing
            href: /solutions/continuous-software-compliance/
            data_ga_name: continuous software compliance learn more
            data_ga_location: body
          - title: Software Supply Chain Security
            description: Ensure your software supply chain is secure and compliant.
            icon:
              name: continuous-delivery
              alt: Continuous Delivery
              variant: marketing
            href: /solutions/supply-chain/
            data_ga_name: software supply chain security learn more
            data_ga_location: body